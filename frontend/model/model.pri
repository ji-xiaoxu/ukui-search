INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/best-list-model.h \
    $$PWD/search-result-manager.h \
    $$PWD/search-result-model.h \
    $$PWD/web-search-model.h

SOURCES += \
    $$PWD/best-list-model.cpp \
    $$PWD/search-result-manager.cpp \
    $$PWD/search-result-model.cpp \
    $$PWD/web-search-model.cpp

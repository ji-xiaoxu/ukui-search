INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/best-list-view.h \
    $$PWD/result-view-delegate.h \
    $$PWD/result-view.h \
    $$PWD/web-search-view.h

SOURCES += \
    $$PWD/best-list-view.cpp \
    $$PWD/result-view-delegate.cpp \
    $$PWD/result-view.cpp \
    $$PWD/web-search-view.cpp

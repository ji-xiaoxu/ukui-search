# ukui-search

[dWIP] UKUI Search is a user-wide desktop search feature of UKUI desktop environment.

Build from source 
 

    git clone https://github.com/ukui/ukui-search.git

    cd ukui-search && mkdir build && cd build

    qmake .. && make

    sudo make install

    /usr/bin/ukui-search


INCLUDEPATH += $$PWD \

HEADERS += \
    $$PWD/action-label.h \
    $$PWD/plugin-iface.h \
    $$PWD/search-plugin-iface.h \
    $$PWD/data-queue.h \
    $$PWD/separation-line.h

SOURCES += \
    $$PWD/action-label.cpp \
    $$PWD/separation-line.cpp


INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/app-match.h \
    $$PWD/app-search-plugin.h

SOURCES += \
    $$PWD/app-match.cpp \
    $$PWD/app-search-plugin.cpp

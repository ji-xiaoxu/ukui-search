<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Zeeker::AppMatch</name>
    <message>
        <location filename="../../libsearch/appsearch/app-match.cpp" line="262"/>
        <source>Application Description:</source>
        <translation>应用描述：</translation>
    </message>
</context>
<context>
    <name>Zeeker::AppSearch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>Zeeker::AppSearchPlugin</name>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="11"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="171"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="12"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="172"/>
        <source>Add Shortcut to Desktop</source>
        <translation>添加到桌面快捷方式</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="13"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="173"/>
        <source>Add Shortcut to Panel</source>
        <translation>添加到任务栏快捷方式</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="14"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="174"/>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="30"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="35"/>
        <source>Applications Search</source>
        <translation>应用搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="106"/>
        <source>Application</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>Zeeker::DirSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="218"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="355"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="219"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="356"/>
        <source>Open path</source>
        <translation>打开文件所在路径</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="220"/>
        <source>Copy Path</source>
        <translation>复制文件路径</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="239"/>
        <source>Dir Search</source>
        <translation>目录搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="234"/>
        <source>Dir search.</source>
        <translation>目录搜索。</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="290"/>
        <source>directory</source>
        <translation>目录</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="330"/>
        <source>Path</source>
        <translation>路径</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="342"/>
        <source>Last time modified</source>
        <translation>上次修改时间</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="357"/>
        <source>Copy path</source>
        <translation>复制路径</translation>
    </message>
</context>
<context>
    <name>Zeeker::FileContengSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="398"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="596"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="399"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="597"/>
        <source>Open path</source>
        <translation>打开文件所在路径</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="400"/>
        <source>Copy Path</source>
        <translation>复制文件路径</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">文本内容搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="414"/>
        <source>File content search.</source>
        <translation>文本内容搜索。</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="419"/>
        <source>File content search</source>
        <translation>文本内容搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="464"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="571"/>
        <source>Path</source>
        <translation>路径</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="583"/>
        <source>Last time modified</source>
        <translation>上次修改时间</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="598"/>
        <source>Copy path</source>
        <translation>复制路径</translation>
    </message>
</context>
<context>
    <name>Zeeker::FileSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="11"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="158"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="12"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="159"/>
        <source>Open path</source>
        <translation>打开文件所在路径</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="13"/>
        <source>Copy Path</source>
        <translation>复制文件路径</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="32"/>
        <source>File Search</source>
        <translation>文件搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="27"/>
        <source>File search.</source>
        <translation>文件搜索。</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="67"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="183"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="69"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="185"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>没有找到默认打开%1的应用。</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="93"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="133"/>
        <source>Path</source>
        <translation>路径</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="145"/>
        <source>Last time modified</source>
        <translation>上次修改时间</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="160"/>
        <source>Copy path</source>
        <translation>复制路径</translation>
    </message>
</context>
<context>
    <name>Zeeker::MailSearch</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="335"/>
        <source>From</source>
        <translation>发件人</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="336"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="337"/>
        <source>To</source>
        <translation>收件人</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="338"/>
        <source>Cc</source>
        <translation>抄送人</translation>
    </message>
</context>
<context>
    <name>Zeeker::MailSearchPlugin</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="29"/>
        <source>open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="38"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="43"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="48"/>
        <source>Mail Search</source>
        <translation>邮件搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="88"/>
        <source>Mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="229"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
</context>
<context>
    <name>Zeeker::NoteSearch</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="190"/>
        <source>Note Description:</source>
        <translatorcomment>便签内容：</translatorcomment>
        <translation>便签内容：</translation>
    </message>
</context>
<context>
    <name>Zeeker::NoteSearchPlugin</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="12"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="128"/>
        <source>Open</source>
        <translatorcomment>打开</translatorcomment>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="31"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="97"/>
        <source>Note Search</source>
        <translatorcomment>便签</translatorcomment>
        <translation>便签</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="26"/>
        <source>Note Search.</source>
        <translatorcomment>便签.</translatorcomment>
        <translation>便签.</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="71"/>
        <source>Application</source>
        <translatorcomment>应用</translatorcomment>
        <translation>应用</translation>
    </message>
</context>
<context>
    <name>Zeeker::SearchManager</name>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="98"/>
        <source>Path:</source>
        <translation>路径：</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="99"/>
        <source>Modified time:</source>
        <translation>修改时间：</translation>
    </message>
</context>
<context>
    <name>Zeeker::SettingsSearchPlugin</name>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="11"/>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="405"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="31"/>
        <source>Settings Search</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="26"/>
        <source>Settings search.</source>
        <translation>设置。</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="391"/>
        <source>Settings</source>
        <translation>设置项</translation>
    </message>
</context>
<context>
    <name>Zeeker::WebSearchPlugin</name>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="7"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="94"/>
        <source>Start browser search</source>
        <translation>启动浏览器搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="19"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="24"/>
        <source>Web Page</source>
        <translation>网页</translation>
    </message>
</context>
</TS>

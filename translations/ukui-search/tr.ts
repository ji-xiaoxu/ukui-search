<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>ContentWidget</name>
    <message>
        <source>Recently Opened</source>
        <translation type="vanished">Yeni Açılan</translation>
    </message>
    <message>
        <source>Open Quickly</source>
        <translation type="vanished">Hızlı Aç</translation>
    </message>
    <message>
        <source>Commonly Used</source>
        <translation type="vanished">Genel olarak kullanılan</translation>
    </message>
    <message>
        <source>Apps</source>
        <translation type="vanished">Uygulamalar</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Ayarlar</translation>
    </message>
    <message>
        <source>Files</source>
        <translation type="vanished">Dosyalar</translation>
    </message>
    <message>
        <source>Dirs</source>
        <translation type="vanished">Dizinler</translation>
    </message>
    <message>
        <source>File Contents</source>
        <translation type="vanished">Dosya İçeriği</translation>
    </message>
    <message>
        <source>Best Matches</source>
        <translation type="vanished">En İyi Eşleşen</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Bilinmeyen</translation>
    </message>
</context>
<context>
    <name>CreateIndexAskDialog</name>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
</context>
<context>
    <name>FolderListItem</name>
    <message>
        <source>Delete the folder out of blacklist</source>
        <translation type="vanished">Klasörü kara listeden silin</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>ukui-search</source>
        <translation type="vanished">ukui-ara</translation>
    </message>
    <message>
        <source>Global Search</source>
        <translation type="vanished">Genel Arama</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Ara</translation>
    </message>
</context>
<context>
    <name>OptionView</name>
    <message>
        <source>Open</source>
        <translation type="vanished">Aç</translation>
    </message>
    <message>
        <source>Add Shortcut to Desktop</source>
        <translation type="vanished">Masaüstüne Kısayol Ekle</translation>
    </message>
    <message>
        <source>Add Shortcut to Panel</source>
        <translation type="vanished">Panele Kısayol Ekle</translation>
    </message>
    <message>
        <source>Open path</source>
        <translation type="vanished">Yolu aç</translation>
    </message>
    <message>
        <source>Copy path</source>
        <translation type="vanished">Yolu kopyala</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../frontend/main.cpp" line="184"/>
        <source>ukui-search is already running!</source>
        <translation>ukui-bul zaten çalışıyor!</translation>
    </message>
</context>
<context>
    <name>SearchBarHLayout</name>
    <message>
        <source>Search</source>
        <translation type="vanished">Ara</translation>
    </message>
</context>
<context>
    <name>SearchDetailView</name>
    <message>
        <source>Path</source>
        <translation type="vanished">Yol</translation>
    </message>
    <message>
        <source>Last time modified</source>
        <translation type="vanished">Son değiştirilme zamanı</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">Uygulama</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="vanished">Belge</translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <source>Search</source>
        <translation type="vanished">Ara</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation type="vanished">Aşağıdaki klasörler aranmayacaktır. Klasör ekleyip kaldırarak ayarlayabilirsiniz.</translation>
    </message>
    <message>
        <source>Add ignored folders</source>
        <translation type="vanished">Göz ardı edilen klasörleri ekleyin</translation>
    </message>
    <message>
        <source>Please select search engine you preferred.</source>
        <translation type="vanished">Lütfen tercih ettiğiniz arama motorunu seçin.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Creating ...</source>
        <translation type="vanished">Oluşturuluyor...</translation>
    </message>
    <message>
        <source>ukui-search</source>
        <translation type="vanished">ukui-bul</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Settings&lt;/h2&gt;</source>
        <translation type="vanished">&lt;h2&gt;Ayarlar&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Index State&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt;Dizin Durumu&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;File Index Settings&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt;Dosya Dizini Ayarları&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Search Engine Settings&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt;SArama Motoru Ayarları&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Whether to delete this directory?</source>
        <translation type="vanished">Bu dizini silinsin mi?</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="vanished">Tamam</translation>
    </message>
    <message>
        <source>Index Entry: %1</source>
        <translation type="vanished">Dizin Girişi: %1</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">Dizinler</translation>
    </message>
    <message>
        <source>select blocked folder</source>
        <translation type="vanished">engellenen klasörü seç</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Seç</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">Pozisyon: </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="vanished">Dosya Adı: </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">Dosya Türü: </translation>
    </message>
</context>
<context>
    <name>ShowMoreLabel</name>
    <message>
        <source>Show More...</source>
        <translation type="vanished">Daha Fazla Göster...</translation>
    </message>
    <message>
        <source>Retract</source>
        <translation type="vanished">Geri çek</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation type="vanished">Yükleniyor</translation>
    </message>
    <message>
        <source>Loading.</source>
        <translation type="vanished">Yükleniyor.</translation>
    </message>
    <message>
        <source>Loading..</source>
        <translation type="vanished">Yükleniyor..</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation type="vanished">Yükleniyor...</translation>
    </message>
</context>
<context>
    <name>Zeeker::BestListWidget</name>
    <message>
        <location filename="../../frontend/view/best-list-view.cpp" line="309"/>
        <source>Best Matches</source>
        <translation type="unfinished">En İyi Eşleşen</translation>
    </message>
</context>
<context>
    <name>Zeeker::ContentWidget</name>
    <message>
        <source>Recently Opened</source>
        <translation type="obsolete">Yeni Açılan</translation>
    </message>
    <message>
        <source>Open Quickly</source>
        <translation type="obsolete">Hızlı Aç</translation>
    </message>
    <message>
        <source>Commonly Used</source>
        <translation type="obsolete">Genel olarak kullanılan</translation>
    </message>
    <message>
        <source>Apps</source>
        <translation type="obsolete">Uygulamalar</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Ayarlar</translation>
    </message>
    <message>
        <source>Files</source>
        <translation type="obsolete">Dosyalar</translation>
    </message>
    <message>
        <source>Dirs</source>
        <translation type="obsolete">Dizinler</translation>
    </message>
    <message>
        <source>File Contents</source>
        <translation type="obsolete">Dosya İçeriği</translation>
    </message>
    <message>
        <source>Best Matches</source>
        <translation type="obsolete">En İyi Eşleşen</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Bilinmeyen</translation>
    </message>
</context>
<context>
    <name>Zeeker::CreateIndexAskDialog</name>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="40"/>
        <source>ukui-search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="66"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="91"/>
        <source>Creating index can help you getting results quickly, whether to create or not?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="102"/>
        <source>Don&apos;t remind</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="113"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="115"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Zeeker::FolderListItem</name>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="538"/>
        <source>Delete the folder out of blacklist</source>
        <translation type="unfinished">Klasörü kara listeden silin</translation>
    </message>
</context>
<context>
    <name>Zeeker::HomePage</name>
    <message>
        <source>Open Quickly</source>
        <translation type="obsolete">Hızlı Aç</translation>
    </message>
    <message>
        <source>Recently Opened</source>
        <translation type="obsolete">Yeni Açılan</translation>
    </message>
    <message>
        <source>Commonly Used</source>
        <translation type="obsolete">Genel olarak kullanılan</translation>
    </message>
</context>
<context>
    <name>Zeeker::MainWindow</name>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="70"/>
        <source>ukui-search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="76"/>
        <source>Global Search</source>
        <translation type="unfinished">Genel Arama</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
</context>
<context>
    <name>Zeeker::OptionView</name>
    <message>
        <source>Open</source>
        <translation type="obsolete">Aç</translation>
    </message>
    <message>
        <source>Add Shortcut to Desktop</source>
        <translation type="obsolete">Masaüstüne Kısayol Ekle</translation>
    </message>
    <message>
        <source>Add Shortcut to Panel</source>
        <translation type="obsolete">Panele Kısayol Ekle</translation>
    </message>
    <message>
        <source>Open path</source>
        <translation type="obsolete">Yolu aç</translation>
    </message>
    <message>
        <source>Copy path</source>
        <translation type="obsolete">Yolu kopyala</translation>
    </message>
</context>
<context>
    <name>Zeeker::SearchBarHLayout</name>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
</context>
<context>
    <name>Zeeker::SearchDetailView</name>
    <message>
        <source>Application</source>
        <translation type="obsolete">Uygulama</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="obsolete">Belge</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="obsolete">Yol</translation>
    </message>
    <message>
        <source>Last time modified</source>
        <translation type="obsolete">Son değiştirilme zamanı</translation>
    </message>
</context>
<context>
    <name>Zeeker::SearchLineEdit</name>
    <message>
        <location filename="../../frontend/control/search-line-edit.cpp" line="55"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
</context>
<context>
    <name>Zeeker::SettingsWidget</name>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="35"/>
        <source>ukui-search-settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="81"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="299"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="503"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="108"/>
        <source>&lt;h2&gt;Settings&lt;/h2&gt;</source>
        <translation type="unfinished">&lt;h2&gt;Ayarlar&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="113"/>
        <source>&lt;h3&gt;Index State&lt;/h3&gt;</source>
        <translation type="unfinished">&lt;h3&gt;Dizin Durumu&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="115"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="117"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="125"/>
        <source>&lt;h3&gt;File Index Settings&lt;/h3&gt;</source>
        <translation type="unfinished">&lt;h3&gt;Dosya Dizini Ayarları&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="127"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation type="unfinished">Aşağıdaki klasörler aranmayacaktır. Klasör ekleyip kaldırarak ayarlayabilirsiniz.</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="136"/>
        <source>Add ignored folders</source>
        <translation type="unfinished">Göz ardı edilen klasörleri ekleyin</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="157"/>
        <source>&lt;h3&gt;Search Engine Settings&lt;/h3&gt;</source>
        <translation type="unfinished">&lt;h3&gt;SArama Motoru Ayarları&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="159"/>
        <source>Please select search engine you preferred.</source>
        <translation type="unfinished">Lütfen tercih ettiğiniz arama motorunu seçin.</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="172"/>
        <source>baidu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="174"/>
        <source>sougou</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="176"/>
        <source>360</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="299"/>
        <source>Whether to delete this directory?</source>
        <translation type="unfinished">Bu dizini silinsin mi?</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="300"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="301"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="359"/>
        <source>Creating ...</source>
        <translation type="unfinished">Oluşturuluyor...</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="362"/>
        <source>Done</source>
        <translation type="unfinished">Tamam</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="370"/>
        <source>Index Entry: %1</source>
        <translation type="unfinished">Dizin Girişi: %1</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="416"/>
        <source>Directories</source>
        <translation type="unfinished">Dizinler</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="417"/>
        <source>select blocked folder</source>
        <translation type="unfinished">engellenen klasörü seç</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="418"/>
        <source>Select</source>
        <translation type="unfinished">Seç</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="419"/>
        <source>Position: </source>
        <translation type="unfinished">Pozisyon: </translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="420"/>
        <source>FileName: </source>
        <translation type="unfinished">Dosya Adı: </translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="421"/>
        <source>FileType: </source>
        <translation type="unfinished">Dosya Türü: </translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="422"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="487"/>
        <source>Choosen path is Empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="491"/>
        <source>Choosen path is not in &quot;home&quot;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="495"/>
        <source>Its&apos; parent folder has been blocked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="499"/>
        <source>Set blocked folder failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="504"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Zeeker::ShowMoreLabel</name>
    <message>
        <source>Show More...</source>
        <translation type="obsolete">Daha Fazla Göster...</translation>
    </message>
    <message>
        <source>Retract</source>
        <translation type="obsolete">Geri çek</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation type="obsolete">Yükleniyor</translation>
    </message>
    <message>
        <source>Loading.</source>
        <translation type="obsolete">Yükleniyor.</translation>
    </message>
    <message>
        <source>Loading..</source>
        <translation type="obsolete">Yükleniyor..</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation type="obsolete">Yükleniyor...</translation>
    </message>
</context>
<context>
    <name>Zeeker::WebSearchWidget</name>
    <message>
        <location filename="../../frontend/view/web-search-view.cpp" line="150"/>
        <source>Web Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
